from enum import Enum
from typing import Any
from typing import Tuple
from typing import Optional
from typing import Callable
from typing import Dict, List
import graphviz
import math

counter: int = 1 # for filenames

def visit(something: Any) -> None:
    print(f'v{something.index}')

class EdgeType(Enum):
    directed = 1
    undirected = 2

class Vertex:
    i: int = 0
    data: Any
    index: int

    def __init__(self, data: Any) -> None:
        self.data = data
        self.index = Vertex.i
        Vertex.i += 1

class Edge:
    source: Vertex
    destination: Vertex
    weight: Optional[float]
    ttype: EdgeType

    def __init__(self, source: Vertex, destination: Vertex, weight: Optional[float], _type: EdgeType) -> None:
        self.source = source
        self.destination = destination
        self.weight = weight
        self.ttype = _type

class Graph:
    adjacencies: Dict[Vertex, List[Edge]]

    def getVertex(self, value: str) -> Vertex:
        for v in self.adjacencies.keys():
            if v.data == value:
                return v

    def __init__(self) -> None:
        self.adjacencies = {}

    def create_vertex(self, data: Any) -> None:
        self.adjacencies[Vertex(data)] = []

    def add_directed_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.__add(EdgeType(1), source, destination, weight)

    def add_undirected_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.__add(EdgeType(2), source, destination, weight)

    def __add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge == edge.directed:
            self.adjacencies[source].append(Edge(source, destination, weight, edge))
        else:
            self.adjacencies[source].append(Edge(source, destination, weight, edge))
            self.adjacencies[destination].append(Edge(destination, source, weight, edge))

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        queue: list = [next(iter(self.adjacencies))]
        visited: list = [next(iter(self.adjacencies))]
        while len(queue) != 0:
            current: Vertex = queue.pop(0)
            visit(current)
            for edge in self.adjacencies[current]:
                destination: Vertex = edge.destination
                if destination not in visited:
                    visited.append(destination)
                    queue.append(destination)

    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.__dfs(next(iter(self.adjacencies)), list(), visit)

    def __dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]) -> None:
        visit(v)
        visited.append(v)
        for neighbour in self.adjacencies[v]:
            if neighbour.destination not in visited:
                self.__dfs(neighbour.destination, visited, visit)

    def __str__(self) -> str:
        result = ''
        keys = list(self.adjacencies.keys())
        for i in range(0, len(keys)):
            result += f'{i}: v{keys[i].index} ----> ['
            tmp = list(self.adjacencies.values())
            for listOfEdges in tmp:
                for edge in listOfEdges:
                    current: Vertex = keys[i]
                    destination: Vertex = edge.destination
                    source: Vertex = edge.source
                    if source == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == destination:
                                result += f'{counter}: '
                        result += f'v{edge.destination.index}, '
                    elif destination == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == source:
                                result += f'{counter}: '
                        result += f'v{edge.source.index}, '

            result = result[:-2]
            result += ']\n'
        return result

    def show(self) -> None:
        global counter
        f = graphviz.Digraph('graph', filename=f'graph{counter}.gv')
        counter += 1
        visited: List = []

        for i in iter(self.adjacencies):
            f.node(f'{i.data}')

        for _list in list(self.adjacencies.values()):
            for e in _list:
                if e in visited:
                    continue
                weight: str = ''
                tttype: str = 'normal'
                if (e.weight):
                    weight = e.weight
                if e.ttype.undirected:
                    tttype = 'none'
                    for n in self.adjacencies[e.destination]:
                        if n.destination == e.source:
                            visited.append(n)

                f.edge(f'{e.source.data}', f'{e.destination.data}', label=f'{weight}', arrowhead = tttype)

        f.view()


def mutual_friends(g: Graph, f0: Any, f1: Any) -> List[Any]:
    v0 = g.getVertex(f0)
    v1 = g.getVertex(f1)
    result: List[Vertex] = []

    for edge0 in g.adjacencies[v0]:
        for edg1 in g.adjacencies[v1]:
            if edge0.destination == edg1.destination:
                result.append(edge0.destination)

    return result

graph1: Graph = Graph()

graph1.create_vertex("A")
graph1.create_vertex("B")
graph1.create_vertex("C")
graph1.create_vertex("D")
graph1.create_vertex("E")
graph1.create_vertex("F")

graph1.add_undirected_edge(graph1.getVertex("A"), graph1.getVertex("B"))
graph1.add_undirected_edge(graph1.getVertex("B"), graph1.getVertex("C"))
graph1.add_undirected_edge(graph1.getVertex("D"), graph1.getVertex("C"))
graph1.add_undirected_edge(graph1.getVertex("D"), graph1.getVertex("E"))
graph1.add_undirected_edge(graph1.getVertex("B"), graph1.getVertex("E"))

graph1.show();

print("Mutual friends for B and D / graph1 ", end = '')
result_graph1 = mutual_friends(graph1, "B", "D")
for v in result_graph1:
    print(v.data, end = ", ")
print()

graph2: Graph = Graph()

graph2.create_vertex("A")
graph2.create_vertex("B")
graph2.create_vertex("C")
graph2.create_vertex("D")
graph2.create_vertex("E")

graph2.add_undirected_edge(graph2.getVertex("A"), graph2.getVertex("B"))
graph2.add_undirected_edge(graph2.getVertex("B"), graph2.getVertex("C"))
graph2.add_undirected_edge(graph2.getVertex("D"), graph2.getVertex("C"))
graph2.add_undirected_edge(graph2.getVertex("D"), graph2.getVertex("E"))

graph2.show();

print("Mutual friends for B and D / graph2: ", end = '')
result_graph2 = mutual_friends(graph2, "B", "D")
for v in result_graph2:
    print(v.data, end = ", ")
print()

graph3: Graph = Graph()

graph3.create_vertex("A")
graph3.create_vertex("B")
graph3.create_vertex("C")
graph3.create_vertex("D")
graph3.create_vertex("E")
graph3.create_vertex("F")
graph3.create_vertex("G")
graph3.create_vertex("H")
graph3.create_vertex("I")
graph3.create_vertex("J")

graph3.add_undirected_edge(graph3.getVertex("A"), graph3.getVertex("B"))
graph3.add_undirected_edge(graph3.getVertex("B"), graph3.getVertex("C"))
graph3.add_undirected_edge(graph3.getVertex("A"), graph3.getVertex("D"))
graph3.add_undirected_edge(graph3.getVertex("A"), graph3.getVertex("E"))
graph3.add_undirected_edge(graph3.getVertex("C"), graph3.getVertex("G"))
graph3.add_undirected_edge(graph3.getVertex("C"), graph3.getVertex("E"))
graph3.add_undirected_edge(graph3.getVertex("D"), graph3.getVertex("H"))
graph3.add_undirected_edge(graph3.getVertex("H"), graph3.getVertex("E"))
graph3.add_undirected_edge(graph3.getVertex("H"), graph3.getVertex("I"))
graph3.add_undirected_edge(graph3.getVertex("I"), graph3.getVertex("J"))
graph3.add_undirected_edge(graph3.getVertex("G"), graph3.getVertex("J"))
graph3.add_undirected_edge(graph3.getVertex("J"), graph3.getVertex("E"))
graph3.add_undirected_edge(graph3.getVertex("E"), graph3.getVertex("B"))
graph3.add_undirected_edge(graph3.getVertex("E"), graph3.getVertex("D"))

graph3.show();

print("Mutual friends for A and E / graph3: ", end = '')
result_graph3 = mutual_friends(graph3, "A", "E")
for v in result_graph3:
    print(v.data, end = ", ")
print()