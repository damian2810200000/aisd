from typing import Any


class Node:
    value: Any
    next: 'Node'

    def __init__(self, value: Any):
        self.value = value
        self.next = None


class LinkedList:
    head: Node
    tail: Node

    def __init__(self):
        self.tail = None
        self.head = None

    def push(self, value: Any) -> None:
        if self.head == None:
            self.head = Node(value)
            self.tail = self.head
        else:
            self.head, self.head.next = Node(value), self.head

    def append(self, value: Any) -> None:
        tmp: Node = self.head

        if tmp == None:
            self.head = Node(value)
            self.tail = self.head
            return None

        while tmp.next != None:
            tmp = tmp.next

        tmp.next = Node(value)
        self.tail = tmp.next

    def node(self, at: int) -> Node:
        tmp: Node = self.head
        for counter in range(0, at):
            tmp = tmp.next
            counter += 1
        return tmp

    def insert(self, value: Any, after: Node) -> None:
        if after.next == None:
            after.next = Node(value)
            self.tail = after.next
        else:
            after.next, (after.next).next = Node(value), after.next

    def pop(self) -> Any:
        result: Node = self.head
        self.head = self.head.next
        return result.value

    def remove_last(self) -> Any:
        tmp1: Node = self.head
        len_: int = len(self)
        result: int = None

        for x in range(0, len_):
            if x == len_ - 2:
                result = (tmp1.next).value
                tmp1.next = None
                self.tail = tmp1
                return result

            tmp1 = tmp1.next

    def remove(self, after: Node) -> None:
        if self.tail == after:
            raise 'exception'

        tmp1: Node = self.head
        while tmp1 != None:
            if tmp1 == after:
                if tmp1.next == self.tail:
                    tmp1.next = None
                    self.tail = tmp1
                else:
                    tmp1.next = (tmp1.next).next
            tmp1 = tmp1.next

    def __str__(self) -> str:
        tmp: Node = self.head
        result: str = str()
        while tmp != None:
            result += str(tmp.value)
            if tmp.next != None:
                result += ' -> '
            tmp = tmp.next

        return str(result)

    def __len__(self) -> int:
        tmp: Node = self.head
        counter: int = 0;
        while tmp != None:
            counter += 1
            tmp = tmp.next

        return counter

list_ = LinkedList()

list_ = LinkedList()

assert list_.head == None

list_.push(1)
list_.push(0)

assert str(list_) == '0 -> 1'

list_.append(9)
list_.append(10)

assert str(list_) == '0 -> 1 -> 9 -> 10'

middle_node = list_.node(at=1)
list_.insert(5, after=middle_node)

assert str(list_) == '0 -> 1 -> 5 -> 9 -> 10'

first_element = list_.node(at=0)
returned_first_element = list_.pop()

assert first_element.value == returned_first_element

last_element = list_.node(at=3)
returned_last_element = list_.remove_last()

assert last_element.value == returned_last_element
assert str(list_) == '1 -> 5 -> 9'

second_node = list_.node(at=1)
list_.remove(second_node)


assert str(list_) == '1 -> 5'