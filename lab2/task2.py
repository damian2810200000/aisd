from task1 import LinkedList 
from typing import Any

class Stack:
    _storage: LinkedList

    def __init__(self):
        self._storage = LinkedList()

    def push(self, element: Any) -> None:
        self._storage.append(element)

    def pop(self) -> Any:
        return self._storage.remove_last()

    def __str__(self) -> str:
        len_ = len(self._storage)
        result: str = str()

        for x in range(len_ - 1, -1, -1):
            result += str((self._storage.node(at=x)).value) + '\n'

        return result
    
    def __len__(self) -> int:
        return len(self._storage)


stack = Stack()

assert len(stack) == 0

stack.push(3)
stack.push(10)
stack.push(1)

assert len(stack) == 3


top_value = stack.pop()

assert top_value == 1

assert len(stack) == 2