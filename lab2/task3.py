from task1 import LinkedList 
from typing import Any

class Queue:
    _storage: LinkedList

    def __init__(self):
        self._storage = LinkedList()

    def peek(self) -> Any:
        return self._storage.head

    def enqueue(self, element: Any) -> None:
        self._storage.append(element)

    def dequeue(self) -> Any:
        return self._storage.pop()

    def __str__(self) -> str:
        len_ = len(self._storage)
        result: str = str()

        for x in range(0, len_):
            if x != len_ - 1:
                result += str((self._storage.node(at=x)).value) + ', '
            else:
                result += str((self._storage.node(at=x)).value)

        return result

    def __len__(self) -> int:
        return len(self._storage)


queue = Queue()

assert len(queue) == 0

queue.enqueue('klient1')
queue.enqueue('klient2')
queue.enqueue('klient3')

assert str(queue) == 'klient1, klient2, klient3'

client_first = queue.dequeue()

assert client_first == 'klient1'
assert str(queue) == 'klient2, klient3'
assert len(queue) == 2