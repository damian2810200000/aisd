from typing import Any
from typing import List


def bubble_sort(_list: List[Any], inverse: bool = False) -> List[Any]:
	for i in range(len(_list)):
		for j in range(i+1, len(_list)):
			if inverse:
				if _list[i] < _list[j]:
					_list[i], _list[j] = _list[j], _list[i]
			else:
				if _list[i] > _list[j]:
					_list[i], _list[j] = _list[j], _list[i]
	return _list

def selection_sort(_list: List[Any], inverse: bool = False) -> List[Any]:
	for i in range(0, len(_list)):
		min_index = i
		for j in range(i+1, len(_list)):
			if inverse:
				if _list[j] > _list[min_index]:
					min_index = j
			else:
				if _list[j] < _list[min_index]:
					min_index = j
		_list[i], _list[min_index] = _list[min_index], _list[i]
	return _list

def insertion_sort(_list: List[Any], inverse: bool = False) -> List[Any]:
	n: int = len(_list)
	for i in range(1, n):
		key = _list[i]
		j = i
		if inverse:
			while j - 1 >= 0 and _list[j - 1] < key:
				_list[j], _list[j - 1] = _list[j - 1], _list[j]
				j -= 1
		else:
			while j - 1 >= 0 and _list[j - 1] > key:
				_list[j], _list[j - 1] = _list[j - 1], _list[j]
				j -= 1

	return _list

_list: List[Any] = [6,3,9,0,4]
print(_list)
print(bubble_sort(_list.copy()))
print(selection_sort(_list.copy()))
print(insertion_sort(_list.copy()))
print()
print(bubble_sort(_list.copy(), True))
print(selection_sort(_list.copy(), True))
print(insertion_sort(_list.copy(), True))