from typing import Any, Callable

def visit(node: 'BinaryNode') -> None:
    print(node.value)
    return None

class BinaryNode:
    value: Any
    left_child: 'BinaryNode'
    right_child: 'BinaryNode'

    def __init__(self, value: Any):
        self.value = value
        self.left_child = None
        self.right_child = None

    def is_leaft(self) -> bool:
        if self.left_child is None and self.right_child is None:
            return True
        else:
            return False

    def add_left_child(self, value: Any):
        if self.left_child is not None:
            raise RuntimeError(F"Binary Node: {self.value} has already left child!")
        else:
            self.left_child = BinaryNode(value)

    def add_right_child(self, value: Any):
        if self.right_child is not None:
            raise RuntimeError(F"Binary Node: {self.value} has already right child")
        else:
            self.right_child = BinaryNode(value)

    def find_ultimate_left_child(self) -> 'BinaryNode':
        if self.left_child is None:
            return self
        else:
            self.left_child.find_ultimate_left_child()

    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        if self.left_child is not None:
            self.left_child.traverse_in_order(visit)
        visit(self)
        if self.right_child is not None:
            self.right_child.traverse_in_order(visit)
        return None

    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        if self.left_child is not None:
            self.left_child.traverse_post_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_post_order(visit)
        visit(self)
        return None

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        visit(self)
        if self.left_child is not None:
            self.left_child.traverse_pre_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_pre_order(visit)
        return None

    def howManyParents(self, goal: 'BinaryNode', counter: int = 0) -> int:
        if self == goal:
            return counter
        if self.left_child is not None:
            result: Union[int, None] = self.left_child.howManyParents(goal, counter+1)
            if type(result) == int:
                return result
        if self.right_child is not None:
            result: Union[int, None] = self.right_child.howManyParents(goal, counter+1)
            if type(result) == int:
                return result


    def __str__(self) -> str:
        return self.value

class BinaryTree:
    root: BinaryNode

    def __init__(self, value: Any):
        self.root = BinaryNode(value)


    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_in_order(visit)
        return None


    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_post_order(visit)
        return None

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_pre_order(visit)
        return None

    def helpStr(self, current: BinaryNode, buffor: str = '') -> str:
        result: str = ''
        howMany: int = 0
        if current.left_child and current.right_child:
            howMany = 2
        elif current.left_child or current.right_child:
            howMany = 1

        for i in range(0, howMany):
            child: BinaryNode = None
            if i == 0 and current.left_child is not None:
                child = current.left_child
            else:
                child = current.right_child

            if i == howMany -1:
                result += buffor + '└── ' + str(child.value) + '\n'
                result += self.helpStr(child, buffor + '    ')
            else:
                result += buffor + '├── ' + str(child.value) + '\n'
                result += self.helpStr(child, buffor + '│   ')
        return result

    def __str__(self) -> str:
        result: str = str()
        current: BinaryNode = self.root
        if current != None:
            result += str(self.root.value) + '\n'
        else:
            result = 'The tree is empty.'

        result += self.helpStr(current)
        return result

tree = BinaryTree(10)

tree.root.add_left_child(9)
tree.root.add_right_child(2)

tree.root.left_child.add_left_child(1)
tree.root.left_child.add_right_child(3)

tree.root.right_child.add_left_child(4)
tree.root.right_child.add_right_child(6)

assert tree.root.value == 10

assert tree.root.right_child.value == 2
assert tree.root.right_child.is_leaft() is False

assert tree.root.left_child.left_child.value == 1
assert tree.root.left_child.left_child.is_leaft() is True
#print("-------------------------------------------------")
#tree.traverse_in_order(visit)
#print("-------------------------------------------------")
#tree.traverse_post_order(visit)
#print("-------------------------------------------------")
#tree.traverse_pre_order(visit)
print(tree)

