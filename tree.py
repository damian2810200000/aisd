from typing import Any, List, Callable, Union
 
def visit(treeNode: 'TreeNode') -> None:
        print(treeNode.value, end='')
 
class TreeNode:
    value: Any
    children: List['TreeNode']
 
    def __init__(self, value: Any, children: List['TreeNode'] = None):
        self.value = value
        self.children = []
 
 
    def is_leaf(self) -> bool:
        if len(self.children) == 0:
            return True
        else:
            return False
 
 
    def add(self, child: 'TreeNode'):
        self.children.append(child)
 
 
    def __str__(self) -> str:
        return str(self.value)
 
    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]) -> None:
        visit(self)
        for i in range(0, len(self.children)):
            self.children[i].for_each_deep_first(visit)
 
    def for_each_level_order(self, visit: Callable[['TreeNode'], None]) -> None:
        visit(self)
        listing: List = list()
        for item in self.children:
            listing.insert(0, item)
        while len(listing) != 0:
            current: TreeNode = listing.pop()
            visit(current)
            for item in current.children:
                listing.insert(0, item)


    def search(self, value: Any) -> Union['TreeNode', None]:
        # Warning: Not optimal solution
        if self.value == value:
            return self

        for i in range(0, len(self.children)):
            current: TreeNode = self.children[i]
            if current.value == value:
                return current
            else:
                current.search(value)
 
 
class Tree:
    root: TreeNode
 
    def __init__(self, value: Any):
        self.root = TreeNode(value)
 
 
    def add(self, value: Any, parent_name: 'TreeNode'):
        tmp: TreeNode = TreeNode(value, None)
        parent_name.add(tmp)


    def helpStr(self, current: TreeNode, buffor: str = '') -> str:
        result: str = ''
        for i in range(0, len(current.children)):
            child: TreeNode = current.children[i]

            if i == len(current.children) -1:
                result += buffor + '└── ' + str(child.value) + '\n'
                result += self.helpStr(child, buffor + '    ')
            else:
                result += buffor + '├── ' + str(child.value) + '\n'
                result += self.helpStr(child, buffor + '│   ')
        return result

    def __str__(self) -> str:
        result: str = str()
        current: TreeNode = self.root
        if current != None:
            result += str(self.root.value) + '\n'
        else:
            result = 'The tree is empty.'

        result += self.helpStr(current)
        return result

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]) -> None:
        self.root.for_each_level_order(visit)
        print()

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]) -> None:
        self.root.for_each_deep_first(visit)
        print()


tree: Tree = Tree('F')

tree.add('B', tree.root)
tree.add('G', tree.root)

tree.add('A', tree.root.children[0])
tree.add('D', tree.root.children[0])
tree.add('I', tree.root.children[1])

tree.add('C', tree.root.children[0].children[1])
tree.add('E', tree.root.children[0].children[1])
tree.add('H', tree.root.children[1].children[0])

print(tree)
searched = tree.root.children[0].children[1].search('D')
if type(searched) == TreeNode:
    print(searched.value)
else:
    print('The specified value does not exist in the node or its children')

print(tree.root.is_leaf())
print(tree.root.children[0].is_leaf())
print(tree.root.children[0].children[0].is_leaf())

tree.for_each_deep_first(visit)

tree.for_each_level_order(visit)