from enum import Enum
from typing import Any
from typing import Tuple
from typing import Optional
from typing import Callable
from typing import Dict, List
import graphviz
import math

def visit(something: Any) -> None:
    print(f'v{something.index}')

class EdgeType(Enum):
    directed = 1
    undirected = 2

class Vertex:
    i: int = 0
    data: Any
    index: int

    def __init__(self, data: Any) -> None:
        self.data = data
        self.index = Vertex.i
        Vertex.i += 1

class Edge:
    source: Vertex
    destination: Vertex
    weight: Optional[float]

    def __init__(self, source: Vertex, destination: Vertex, weight: Optional[float]) -> None:
        self.source = source
        self.destination = destination
        self.weight = weight

class Graph:
    adjacencies: Dict[Vertex, List[Edge]]

    def __init__(self) -> None:
        self.adjacencies = {}

    def create_vertex(self, data: Any) -> None:
        self.adjacencies[Vertex(data)] = []

    def add_directed_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.__add(EdgeType(1), source, destination, weight)

    def add_undirected_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.add(EdgeType(2), source, destination, weight)

    def __add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge.directed:
            self.adjacencies[source].append(Edge(source, destination, weight))
        else:
            self.adjacencies[source].append(Edge(source, destination, weight))
            self.adjacencies[destination].append(Edge(source, destination, weight))

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        queue: list = [next(iter(self.adjacencies))]
        visited: list = [next(iter(self.adjacencies))]
        while len(queue) != 0:
            current: Vertex = queue.pop(0)
            visit(current)
            for edge in self.adjacencies[current]:
                destination: Vertex = edge.destination
                if destination not in visited:
                    visited.append(destination)
                    queue.append(destination)

    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.__dfs(next(iter(self.adjacencies)), list(), visit)

    def __dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]) -> None:
        visit(v)
        visited.append(v)
        for neighbour in self.adjacencies[v]:
            if neighbour.destination not in visited:
                self.__dfs(neighbour.destination, visited, visit)

    def __str__(self) -> str:
        result = ''
        keys = list(self.adjacencies.keys())
        for i in range(0, len(keys)):
            result += f'{i}: v{keys[i].index} ----> ['
            tmp = list(self.adjacencies.values())
            for listOfEdges in tmp:
                for edge in listOfEdges:
                    current: Vertex = keys[i]
                    destination: Vertex = edge.destination
                    source: Vertex = edge.source
                    if source == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == destination:
                                result += f'{counter}: '
                        result += f'v{edge.destination.index}, '
                    elif destination == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == source:
                                result += f'{counter}: '
                        result += f'v{edge.source.index}, '

            result = result[:-2]
            result += ']\n'
        return result

    def show(self) -> None:
        f = graphviz.Digraph('graph', filename='file1.gv')

        for i in iter(self.adjacencies):
            f.node(f'{i.data}')

        for _list in list(self.adjacencies.values()):
            for e in _list:
                f.edge(f'{e.source.data}', f'{e.destination.data}', label=f'{e.weight}')

        f.view()


class GraphPath:
    __graph: Graph
    __start: Vertex
    __end: Vertex
    __path: List[Vertex]

    def __init__(self, graph: Graph, start: Vertex, end: Vertex) -> None:
        self.graph = graph
        self.start = start
        self.end = end

    def __weighted(self) -> bool:
        if (self.graph.adjacencies[self.start])[0].weight:
            return True
        else:
            return False

    def find(self) -> None:
        if self.__weighted():
            self.__dijkstra()
        else:
            self.__breadth()

    def __breadth(self) -> None:
        queue: List[Edge] = [(self.graph.adjacencies[self.start])[0]]
        visited: List[Vertex] = [(self.graph.adjacencies[self.start])[0].destination]
        while (len(queue) != 0):
            currentPath: Edge = queue.pop(0)
            currentVertex: Vertex = currentPath.destination
            for np in self.graph.adjacencies[currentVertex]:
                if np.destination in visited:
                    continue
                else:
                    queue.append(np)
                    visited.append(np.destination)
                    if np.destination is self.end:
                        self.__path = visited
                        return None

    def __dijkstra(self) -> None:
        costs: Dict[Vertex, int] = dict()
        parents: Dict[Vertex, Vertex] = dict()
        visited: list[Vertex] = [self.start]

        costs[self.end] = math.inf

        for edge in self.graph.adjacencies[self.start]:
            costs[edge.destination] = edge.weight

        for vertex in list(costs.keys()):
            if costs[vertex] is math.inf:
                parents[vertex] = None
            else:
                parents[vertex] = self.start

        cheapestPackage: Tuple = self.__cheapestNewVertex(costs, visited)
        while(cheapestPackage[0]):
            self.debug(costs, parents)
            for edge in self.graph.adjacencies[cheapestPackage[0]]:
                nc: int = cheapestPackage[1] + edge.weight
                if costs[edge.destination] > nc:
                    costs[edge.destination] = nc
                    parents[edge.destination] = cheapestPackage[0]
            visited.append(cheapestPackage[0])
            cheapestPackage = self.__cheapestNewVertex(costs, visited)

        parent: Vertex = parents[self.end]
        path: List = []
        while parent is not self.start:
            path.append(parent)
            parent = parents[parent]
        path.append(self.start)
        self.__path = path

    def debug(self, costs: Dict[Vertex, int], parents: Dict[Vertex, Vertex]) -> None:
        print('-------------------------------------------------------------')
        for vertex in costs.keys():
            print(f'{str(vertex.data)} | {str(costs[vertex])} | ', end='')
            tmp = parents[vertex]
            if (type(tmp) == Vertex):
                print(f'{str(parents[vertex].data)}')
            else:
                print('None')

    def __cheapestNewVertex(self, _list: Dict[Vertex, int], visited: List[Vertex]) -> Tuple[Vertex, int]:
        result: Vertex = None
        for vertex in _list.keys():
            if vertex not in visited:
                result = vertex

        if result is None:
            return (None, None)

        lc: int = _list[result]

        for v in _list.keys():
            if v not in visited:
                if _list[v] < lc:
                    lc = _list[v]
                    result = v
        return result, lc

    def show(self) -> None:
        f = graphviz.Digraph('graph', filename='file2.gv')

        counter: int = 0
        for i in iter(self.graph.adjacencies):
            if self.start is i:
                f.node(f'{i.data}', color = "red", label = "start")
            elif self.end is i:
                f.node(f'{i.data}', color = "blue", label = "end")
            elif i in self.__path:
                f.node(f'{i.data}', style = "filled", color = "yellow")
                counter += 1
            else:
                f.node(f'{i.data}')

        for _list in list(self.graph.adjacencies.values()):
            for e in _list:
                f.edge(f'{e.source.data}', f'{e.destination.data}', label=f'{e.weight}')

        f.view()


graph1: Graph = Graph()

graph1.create_vertex("A")
graph1.create_vertex("B")
graph1.create_vertex("C")
graph1.create_vertex("D")

vertices = list(graph1.adjacencies.keys())

graph1.add_directed_edge(vertices[0], vertices[1], 30)
graph1.add_directed_edge(vertices[0], vertices[2], 10)
graph1.add_directed_edge(vertices[2], vertices[1], 5)
graph1.add_directed_edge(vertices[1], vertices[3], 2)
graph1.add_directed_edge(vertices[2], vertices[3], 9)

print(graph1)
graphPath1: GraphPath = GraphPath(graph1, vertices[0], vertices[3])
graphPath1.find()
graph1.show()
graphPath1.show()

#graph2: Graph = Graph()

#graph2.create_vertex("a")
#graph2.create_vertex("b")
#graph2.create_vertex("c")
#graph2.create_vertex("d")
#graph2.create_vertex("e")
#graph2.create_vertex("f")

#vertices = list(graph2.adjacencies.keys())

#graph2.add_directed_edge(vertices[0], vertices[1])
#graph2.add_directed_edge(vertices[0], vertices[5])
#graph2.add_directed_edge(vertices[2], vertices[1])
#graph2.add_directed_edge(vertices[2], vertices[3])
#graph2.add_directed_edge(vertices[3], vertices[4])
#graph2.add_directed_edge(vertices[4], vertices[1])
#graph2.add_directed_edge(vertices[4], vertices[5])
#graph2.add_directed_edge(vertices[5], vertices[1])
#graph2.add_directed_edge(vertices[5], vertices[2])

#print(graph2)
#graphPath2: GraphPath = GraphPath(graph2, vertices[3], vertices[2])
#graphPath2.find()
#graph2.show();
#graphPath2.show()