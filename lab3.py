from typing import List

def numbers(n: int) -> None:
	print(n)
	if n == 0:
		return None
	numbers(n-1)

def fib(n: int) -> int:
	if n == 0:
		return 0
	elif n == 1:
		return 1
	else:
		return fib(n-1) + fib(n-2)

def power(number: int, n: int) -> int:
	if n == 1:
		return number
	tmp: int = power(number, n-1)
	return number * tmp

def reverse(txt: str) -> str:
	if len(txt) == 1:
		return txt
	tmp1 = reverse(txt[1:])
	tmp2 = txt[0:1]
	return tmp1 + tmp2

def factorial(n: int) -> int:
	if n == 0:
		return 1
	return n * factorial(n-1)

def helpPrime(n, i) -> bool:
	if n == i:
		return True
	else:
		if n % i == 0:
			return False
		else:
			return helpPrime(n, i + 1)

def prime(n: int) -> bool:
	return helpPrime(n, 2)


def maks(n: int = 1, i: int = 1) -> int:
	if i == 0:
		return n
	else:
		return maks(n*10, i-1)

def bEvenEqualOdd(string: str, i: int, evenSum: int = 0, oddSum: int = 0) -> bool:
	if i == 0:
		if evenSum == oddSum:
			return True
		else:
			return False
	
	if i % 2 == 0:
		evenSum += int(string[i-1])
	else:
		oddSum += int(string[i-1])
	
	return bEvenEqualOdd(string, i - 1, evenSum, oddSum)

def checkNumbers(min: int, max: int, result: List[int]) -> List[int]:
	if min > max:
		return result
	else:
		string: str = str(min)
		if bEvenEqualOdd(string, len(string)):
			result.append(string)
		return checkNumbers(min+1, max, result)


def n_sums(n: int) -> List[int]:
	result: list[int] = list()
	makss = maks(i=n)
	return checkNumbers(int(makss/10), makss, result)


def inString(string: str, result: str) -> str:
	if len(string) == 0:
		return result
	else:
		if string[0] not in result:
			result.append(string[0])
		string = string[1:]
		return inString(string, result)

def remove_duplicates(txt: str) -> str:
	result: list = list()
	inString(txt, result)
	return ''.join(result)

def hbp(string: List, i: int, n: int, open: int, close: int, result: List[List[int]] = list()) -> None:
    if (close == n):
        result.append(string)
        return;
    else:
        if (open > close):
            string[i] = ')'
            hbp(string, i + 1, n, open, close + 1, result)
        if (open < n):
            string[i] = '('
            hbp(string, i + 1, n, open + 1, close, result)

def balanced_parentheses(n: int) -> str:
	string: list = [''] * n
	result: List[List[int]] = list()
	hbp(string, 0, n/2, 0, 0, result)
	for x in result:
		for y in x:
			print(y, end="")
		print('');

def canAdd(tmp: list, current: int, i: int) -> bool:
	if tmp[i] == '' and tmp[i + current + 1] == '':
		return True
	else:
		return False


def permutations(string: str, tmp: list, current: int, result: List[List[int]]) -> None:
	for i in range(0, len(string)):
		
		if (canAdd(tmp, current, i)):
			tmp[i] = current
			tmp[i + current + 1] = current

			b: bool = True
			for p in tmp:
				if p == '':
					b = False
			if b:
				result.append(tmp.copy())

			permutations(string, tmp, current + 1, result)

			tmp[i] = ''
			tmp[i + current + 1] = ''
			
def combinations(n: int) -> List[List[int]]:
	numbers: List[List[int]] = list()
	string: str = ''.join([str(elem) for elem in range(1, n+1)])
	permutations(string, [''] *2* n, 1, numbers)
	sureResult: List[List[int]] = list()
	for x in numbers:
		if sureResult.count(x) == 0:
			sureResult.append(x)
	return sureResult


#numbers(10)
#print(fib(10))
#print(power(8, 3))
#print(reverse("jel"))
#print(factorial(4))
#print(prime(15))
#print(n_sums(3))
#print(balanced_parentheses(4))
#print(remove_duplicates("XXYZZZ"))
print(combinations(3))