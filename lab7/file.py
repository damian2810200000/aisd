from enum import Enum
from typing import Any, KeysView
from typing import Optional
from typing import Callable
from typing import Dict, List
import graphviz

def visit(something: Any) -> None:
    print(f'v{something.index}')

class EdgeType(Enum):
    directed = 1
    undirected = 2

class Vertex:
    i: int = 0
    data: Any
    index: int

    def __init__(self, data: Any) -> None:
        self.data = data
        self.index = Vertex.i
        Vertex.i += 1

class Edge:
    source: Vertex
    destination: Vertex
    weight: Optional[float]

    def __init__(self, source: Vertex, destination: Vertex, weight: Optional[float]) -> None:
        self.source = source
        self.destination = destination
        self.weight = weight

class Graph:
    adjacencies: Dict[Vertex, List[Edge]]

    def __init__(self) -> None:
        self.adjacencies = {}


    def create_vertex(self, data: Any) -> None:
        self.adjacencies[Vertex(data)] = []


    def add_directed_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.__add(EdgeType(1), source, destination, weight)


    def add_undirected_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        self.add(EdgeType(2), source, destination, weight)

    def __add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge.directed:
            self.adjacencies[source].append(Edge(source, destination, weight))
        else:
            self.adjacencies[source].append(Edge(source, destination, weight))
            self.adjacencies[destination].append(Edge(source, destination, weight))

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        queue: list = [next(iter(self.adjacencies))]
        visited: list = [next(iter(self.adjacencies))]
        while len(queue) != 0:
            current: Vertex = queue.pop(0)
            visit(current)
            for edge in self.adjacencies[current]:
                destination: Vertex = edge.destination
                if destination not in visited:
                    visited.append(destination)
                    queue.append(destination)


    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.__dfs(next(iter(self.adjacencies)), list(), visit)


    def __dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]) -> None:
        visit(v)
        visited.append(v)
        for neighbour in self.adjacencies[v]:
            if neighbour.destination not in visited:
                self.__dfs(neighbour.destination, visited, visit)


    def __str__(self) -> str:
        result = ''
        keys = list(self.adjacencies.keys())
        for i in range(0, len(keys)):
            result += f'{i}: v{keys[i].index} ----> ['
            tmp = list(self.adjacencies.values())
            for listOfEdges in tmp:
                for edge in listOfEdges:
                    current: Vertex = keys[i]
                    destination: Vertex = edge.destination
                    source: Vertex = edge.source
                    if source == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == destination:
                                result += f'{counter}: '
                        result += f'v{edge.destination.index}, '
                    elif destination == current:
                        for counter in range(0, len(keys)):
                            if keys[counter] == source:
                                result += f'{counter}: '
                        result += f'v{edge.source.index}, '

            result = result[:-2]
            result += ']\n'
        return result


    def show(self) -> None:
        f = graphviz.Digraph('graph', filename='file.gv')

        for i in iter(self.adjacencies):
            f.node(f'v{i.index}')

        for _list in list(self.adjacencies.values()):
            for e in _list:
                f.edge(f'v{e.source.index}', f'v{e.destination.index}', label=f'{e.weight}')

        f.view()

graph: Graph = Graph()

graph.create_vertex("a")
graph.create_vertex("b")
graph.create_vertex("c")
graph.create_vertex("d")
graph.create_vertex("e")
graph.create_vertex("f")

vertices = list(graph.adjacencies.keys())

graph.add_directed_edge(vertices[0], vertices[1])
graph.add_directed_edge(vertices[0], vertices[5])
graph.add_directed_edge(vertices[2], vertices[1])
graph.add_directed_edge(vertices[2], vertices[3])
graph.add_directed_edge(vertices[3], vertices[4])
graph.add_directed_edge(vertices[4], vertices[1])
graph.add_directed_edge(vertices[4], vertices[5])
graph.add_directed_edge(vertices[5], vertices[1])
graph.add_directed_edge(vertices[5], vertices[2])

graph.traverse_breadth_first(visit)
print()
graph.traverse_depth_first(visit)
print(graph)
graph.show()