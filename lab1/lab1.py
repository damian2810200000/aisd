from typing import Callable

def foo1(name: str, secondName: str) -> None:
    print(name[0] + "." + secondName)


def foo2(name: str, secondName: str) -> None:
    print(name[0].upper() + "." + secondName[0].upper() + secondName[1:len(secondName)])


def foo3(first: int, second: int, age: int) -> None:
    tmp: int = first * 100 + second
    result: int = tmp - age
    print(f"Your birth age: {result}")


def foo4(name: str, secondName: str, foo: Callable) -> None:
    foo(name, secondName)


def foo5(first: int, second: int) -> float:
    if((first > 0) and (second > 0) and (second != 0)):
        return first / second


def foo6()->None:
    sum: int = 0
    while(sum < 100):
        sum += int(input())


def foo7(li: list)->tuple:
    return tuple(li)


def foo8()->tuple:
    print("How many elements do you want in list?\n: ")
    many: int = int(input())
    result: list = list()
    for x in range(0, many):
        result.append(input("Insert: "))

    return tuple(result)


def foo9(c: int)->str:
    week: list = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    return week[c]

def foo10(input: str)->bool:
    for x in range(0, len(input)):
        if input[x] != input[len(input)-1-x]:
            return False
    return True

# print("Insert name: ")
# name: str = input()
# print("Insert secondName: ")
# secondName: str = input()
# foo1(name, secondName)
# foo2(name, secondName)

# first: int = 20
# second: int = 10
# age: int = 30
# foo3(first, second, age)

# name: str = "damian"
# secondName: str = "tomczak"
# foo4(name, secondName, foo2)

# print(f'Result: {foo5(1, 1)}');

# foo6()

# li: list = [1,2,3,4,5,6]
# tup: tuple = foo7(li)

# print(foo8())

# print(foo9(int(input())))

# print(foo10("okko"))

